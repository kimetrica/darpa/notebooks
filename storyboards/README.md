## Storyboards

Demonstration notebooks for interactively viewing model outputs that could
feed into an early warning system.

## Setup

Follow the [guidelines](https://gitlab.com/kimetrica/darpa/darpa/-/commit/b1540f700a40607b5b5a6bd0327dca8e967e276f) at this repo to set up Docker, and run the notebooks inside the `drp_jupyter_1` container, 
which comes with Voila for html and interative rendering. Make sure the `.yml` files all have the same version number 2.4.

If `drp_jupyter_1` is not running for some reason, it might be necessary to make changes to `docker-compose.jupyter.yml`. It's suggested to silence `${HOME}:/root` under `volumes`, and change the `working_dir` to `/usr/src/app`.

Also, `qgrid` is required to render interactive maps successfully. You might need to install it inside Jupyter Lab if the widget is not rendering.

## Notebook with ipywidgets

The `population_interactive.ipynb` notebook is an example that runs the population model based on inputs of year and country level. The user selects input parameters using 
Dropdown menu and integer sliders. The notebook in this repo doesn't display those UI elements properly, but it looks like this when it is run. Note that
`widgets.interactive` requires a function that processes the value of the slider object.

<div style="text-align:center" markdown="1"><img src="population_ipywidget.png" width="75%" height="75%"></div>

The parameters, such as `country_wd.value`, are passed to the terminal task like this:

```python
pop_hires = HiResPopRasterMasked(
    time=DateIntervalParameter().parse(timepoint),
    country_level=country_wd.value,
    geography=eth_geo_file,
    rainfall_scenario="normal",
    rainfall_scenario_geography=eth_geo_file,
)
```

## Notebook with Interactive Map (Voila)

A Jupyter notebook can be rendered by Voila to provide features such as displays of interactive map and charts in a HTML context. In this repo, the 
notebook `malnutrition_interactive.ipynb` needs to be run first, and it saves a GeoJSON file in the volume of the docker container. 
The notebook `malnutrition_mapping.ipynb`, takes that GeoJSON file, reads it as a `geopandas.GeoDataFrame` to generate a map that returns a tooltip
box when the mouse is hovered on the AOI.

When the notebook is ready, simply hit that Voila icon on the top menu bar as shown below. The map object (in this case `m`) is ready for rendering if you see "Loading widget",
otherwise, fix any error before Voila rendering.

<div style="text-align:center" markdown="1"><img src="loading_widget.png" width="75%" height="75%"></div>

For this map, it comes with a drop down bar that allows the user to choose the Month, and it will update the map content accordingly. This is accomplished 
by the callback function `on_click`:

```python
def on_click(change):
    global month_str
    
    month_str = change['new']
    update_gdf(month_str)
```

<div style="text-align:center" markdown="1"><img src="ipyleaflet_snapshot.png" width="60%" height="60%"></div>

