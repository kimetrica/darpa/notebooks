## Notebooks

A public repository for sharing Jupyter notebooks. For better rendering than GitLab's native view, use https://nbviewer.jupyter.com.

## Jupyter

The repository contains configuation for a docker container that runs Jupyter. It is intended to run on top of the `DARPA/darpa` services. These setup instructions assume you have a clone of the `DARPA/darpa` project, its `.env` file assigns `COMPOSE_PROJECT_NAME=drp` (if not, make a matching substitutuion below), and that the images have all been built and tagged.

1. Build the jupyter image from this repo:

```bash
notebooks$ COMPOSE_PROJECT_NAME=drp docker-compose build
```

2. Add this repo's docker-compose.yml to the `COMPOSE_FILE` list in your `.env`
3. Start the services as usual from the `DARPA/darpa` root:

```bash
darpa$ docker-compose up -d
Creating network "drp_default" with the default driver
Creating drp_redis_1     ... done
Creating drp_db_1      ... done
Creating drp_jupyter_1   ... done
Creating drp_scheduler_1  ... done
Creating drp_beat_1       ... done
Creating drp_controller_1 ... done
Creating drp_celery_1     ... done
```
